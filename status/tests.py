from django.test import TestCase, Client, LiveServerTestCase
import datetime
from django.urls import resolve, reverse
from status import views
from .views import status
from .models import create_status

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time



class StatusTestCase(TestCase):
    def test_about_url_exists(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_about_using_about_func(self):
        found = resolve(reverse('status:status'))
        self.assertEqual(found.func, views.status)

    def test_about_using_about_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'status.html')

    def test_model_can_add_new_status(self):
        new_status = create_status.objects.create(statusfield='ya allah',time=datetime.datetime.now())
        counting_all_status = create_status.objects.all().count()
        self.assertEqual(counting_all_status,1)

    def test_can_save_a_POST_request(self):
        new_status = create_status.objects.create(statusfield='deh hadehh',time=datetime.datetime.now()) 
        response = self.client.post(reverse('status:status'),{'statusfield':'deh hadehh','time':'2001-01-01 01:01'})
        self.assertEqual(response.status_code, 200)

        self.assertEqual(str(new_status),new_status.statusfield)

        html_response= response.content.decode('utf8')
        self.assertIn('deh hadehh',html_response)


class Lab6FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--window-size=1420,1080')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.selenium = webdriver.Chrome(chrome_options=chrome_options)

    def tearDown(self):
        self.selenium.quit()
        super(Lab6FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')
        
        # find the form element
        status = selenium.find_element_by_id('statusfield')
        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        status.send_keys('lelah beb')

        # submitting the form
        submit.send_keys(Keys.RETURN)
        self.assertIn('lelah beb',self.selenium.page_source)
