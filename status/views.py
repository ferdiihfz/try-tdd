from django.shortcuts import render, redirect
from .models import create_status
import datetime

def status(request):
    all_status=create_status.objects.all().order_by('time')
    context= {'all_status': all_status}
    if request.method == 'POST':
        status=create_status()
        status.statusfield=request.POST.get('statusfield')
        status.time=datetime.datetime.now()
        status.save()
        return render(request, 'status.html',context)
    else:
        return render(request, 'status.html',context)
