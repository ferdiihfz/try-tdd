from django.db import models

class create_status(models.Model):
    statusfield = models.CharField(max_length=300)
    time = models.DateTimeField()

    def __str__(self):
        return self.statusfield
